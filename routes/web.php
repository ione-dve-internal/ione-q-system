<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('index');
//});
Route::resource('/', 'IndexController');

//Route::get('invoice', 'IndexController@generateInvoice');
//Route::get('user/invoice/{invoice}', function (\Illuminate\Http\Request $request, $invoiceId) {
//    return $request->user()->downloadInvoice($invoiceId, [
//        'vendor'  => 'Your Company',
//        'product' => 'Your Product',
//    ]);
//});