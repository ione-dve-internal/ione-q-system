(function () {

    var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    function dateFormat(d) {
        var t = new Date(d);
        return t.getDate() + ' ' + monthNames[t.getMonth()] + ', ' + t.getFullYear();
    }
    function dateFormat1(d){
        var t = new Date(d);
        return t.getFullYear()+ '-' + (t.getMonth()+1)+'-'+t.getDate();
    }
    $('#currentDate').text(dateFormat(new Date()));
    $('#date').val(dateFormat1(new Date()));


    // function getCurrentTime(){
    //     var d = new Date(),
    //         h = (d.getHours()<10?'0':'') + d.getHours(),
    //         m = (d.getMinutes()<10?'0':'') + d.getMinutes(),
    //         s = (d.getSeconds()<10?'0':'') + d.getSeconds();
    //
    //
    //     //convert 24h to 12h format
    //     var timeString = h + ' : ' + m + ' : ' + s;
    //     var H = +timeString.substr(0, 2);
    //     var h = (H % 12) || 12;
    //     var ampm = H < 12 ? "AM" : "PM";
    //     timeString = h + ' : ' + m + ' : '+ s + timeString.substr(2, 3) + ampm;
    //     //$('#currentTime').text(timeString);
    // }



    // setTimeout(function(){localStorage.removeItem("txtNumber");}, 1000*60*60); // this is the trick, clear every one hour
    // let sessionNum = 0;
    // if(localStorage.getItem('txtNumber')!==null){
    //     sessionNum = parseInt(localStorage.getItem('txtNumber'));
    //     $('#txtNumber').text(sessionNum);
    //     $('#number').val(sessionNum);
    //     $('body').on('click', '#mybtn', function () {
    //         sessionNum+=1;
    //         $('#txtNumber').text(sessionNum);
    //         $('#number').val(sessionNum);
    //         localStorage.setItem('txtNumber', sessionNum);
    //         $('#btnSubmit').click();
    //     })
    // }else{
    //     $('#txtNumber').text(sessionNum);
    //     $('body').on('click', '#mybtn', function () {
    //         sessionNum+=1;
    //         $('#txtNumber').text(sessionNum);
    //         $('#number').val(sessionNum);
    //         localStorage.setItem('txtNumber', sessionNum);
    //         $('#btnSubmit').click();
    //
    //     })
    // }
    function print() {
        var divToPrint=document.getElementById("invoice");
        newWin= window.open("");
        newWin.document.write('<html><head>');
        // newWin.document.write(divToPrint.outerHTML);

        newWin.document.write('</head><body ><center style="border: 1px solid lightgrey; padding-top: 100px;">');
        newWin.document.write(divToPrint.outerHTML);
        newWin.document.write('</center></body></html>');
        newWin.print();
        newWin.close();
    }
    $('body').on('click', '#invoice', function () {
        var number = $('#txtNumber').text();
        $('#number').val(number);
        print();
       $('#btnSubmit').click();
    })


})()