<?php

namespace App\Http\Controllers;

use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Parse\ParseException;
use Parse\ParseObject;
use Parse\ParseQuery;

include('ConfigParse-sdk.php');

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $date = date("Y-m-d");

        $startDate=new \DateTime($date);
        $startDate->format('Y-m-d H:i:s');
        $startDate->setTime(0,0,0);
        $endDate=new \DateTime($date);
        $endDate->format('Y-m-d H:i:s');
        $endDate->setTime(23,59,59);


        $latestNum = new ParseQuery('Qsystem');
        $latestNum->greaterThanOrEqualTo('createdAt', $startDate);
        $latestNum->lessThanOrEqualTo('createdAt', $endDate);
        $latestNum->descending('createdAt');
        $latestNum->limit(1);

        $result = $latestNum->first();
        if(count($result)!==0){
            $result = $result;
        }else{
            $result = 0;
        }


        return view('index', compact('result', $result));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $number = (int)$request->get('number'); //$number = $number + 1;
        $d = $request->get('date');
        $date=new \DateTime($d);
        $date->format('Y-m-d H:i:s');
        $time = $request->get('time');

        try{
            $qsystem = new ParseObject('Qsystem');
            $qsystem->set('number', $number);
            $qsystem->set('date', $date);
            $qsystem->set('time', $time);
            $qsystem->save();
            return redirect()->back()->with('message', 'Successfully checked in!');
        }catch (ParseException $ex){
            echo $ex->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function generateInvoice(Request $request){
        return $request->user()->downloadInvoice('jdh12s', [
            'vendor'  => 'Your Company',
            'product' => 'Your Product',
        ]);
    }
}
