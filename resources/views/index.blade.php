<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Q-system</title>

    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link href="{{url('bootstrap4/bootstrap.min.css')}}" rel="stylesheet" type="text/css">

    <script>
        function startTime() {
            var today = new Date();
            var h = today.getHours();
            var m = today.getMinutes();
            var s = today.getSeconds();
            m = checkTime(m);
            s = checkTime(s);

            //convert 24h to 12h format
            var timeString = h + ' : ' + m + ' : ' + s;
            var H = +timeString.substr(0, 2);
            var h = (H % 12) || 12;
            var ampm = H < 12 ? 'AM' : 'PM';

            document.getElementById('currentTime').innerHTML =
                h + ' : ' + m + ' : ' + s +' '+ampm;
            document.getElementById('time').value =
                h + ' : ' + m + ' : ' + s +' '+ampm;
            var t = setTimeout(startTime, 500);
        }
        function checkTime(i) {
            if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
            return i;
        }

    </script>

</head>
<body onload="startTime()">
<div class="container" style="padding-top: 50px;">
    @if(session()->has('message'))
        <div class="alert alert-success" id="successMessage">
            {{ session()->get('message') }}
        </div>
    @endif
        <script>
            setTimeout(function () {
                $('#successMessage').fadeOut('fast');

            },500);
        </script>
        {{--{{var_dump(sizeof($result), exit())}}--}}
        <div class="col-md-10 mx-auto">
            <div class="card">
                <div class="card-header" style="font-family: 'Times New Roman'">
                    iOne Check In System
                </div>
                <div class="card-body">
                    <div class="row" >
                        <div class="col-md-8 mx-auto">
                            <div class="card">
                                <div class="card-body" id="invoice" style="cursor: pointer;">
                                    <div class="row">
                                        <div class="col-md-8 mx-auto text-center">Date : <span id="currentDate"></span></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8 mx-auto text-center">Time : <span id="currentTime"></span></div>
                                    </div>
                                    <p class="card-text text-center" id="txtNumber" style="font-size: 150px;font-family: 'Times New Roman'">
                                        {{$result!==0?$result->get('number')+1:$result}}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <form method="post" action="{{url('/')}}" hidden>
                    {{ csrf_field() }}
                    <input type="text" id="number" name="number">
                    <input type="text" id="date" name="date">
                    <input type="text" id="time" name="time">
                    <button type="submit" id="btnSubmit">Submit</button>
                </form>
                <input type="button" class="button" id="btnPrint" value="Print" onclick="print()" hidden>
            </div>
            <footer>
                <div class="row">
                    <div class="col-md-6 mx-auto" style="padding-top: 100px;">
                        <p class="text-center">iOne Cambodia</p>
                        <p class="text-center">All right reserve &copy; 2019</p>
                    </div>
                </div>
            </footer>
        </div>

</div>
<script src="{{url('js/jquery.min.js')}}"> </script>
<script src="{{url('js/index.js')}}"> </script>

</body>
</html>